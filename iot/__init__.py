""" IoT-RabbitMQ package """

from .core.stdMessage import IoTMsg
from .core.iotactions import Actions
from .ticket.busTicket import TicketValue


__version__ = '0.0.3'
