import configparser
import logging
from nameko.standalone.rpc import ClusterRpcProxy

logger = logging.getLogger(__name__)
CONFIG = {'AMQP_URI': "amqp://guest:guest@localhost:5672"}

class IoTMsg:
    """
    Parameters
    ----------

    Returns
    -------

    Raises
    ------
    """

    def __init__(self):
        self.header = None
        self.body = None
        self.coord_gps = None
        self.response = {}
        self.actions_list = ["ticket_value",
                            "get_pos",
                            "get_next_station"]
        self.stdDict = {
                    "header": {
                        "action": "",
                        "etag": "",
                        "code": "",
                        "document_datetime_init": ""
                    },
                    "body": {},
                    "api_timespan": ""}
        self.fareDict = {
                    "Normal": None,
                    "Student": None,
                    "Elderly":None}
        self.services = ClusterRpcProxy(CONFIG)

    def _load_config(self):
        """
        Parameters
        ----------
        config: load api.cfg

        Returns
        -------
        Configuration Parameters:
            filters: int
                low, high, QRSWinL, QRSWinR, FirWin, std_fs,
            QRS Cluster: int
                maxCluster, minCluster
        Raises
        ------
        ServerSelectionTimeoutError, error: 1
        """
        config = configparser.ConfigParser()
        config.read('./api.cfg')



## Example
# {
# "header": {
# "action": "iotrabbitmq.api.value_ticket",
# "etag": "a41d6cc6d92b52a9ae8f127b9f335bdac8af9dcebaf847c02ba489ff",
# "code": "000",
# "document_datetime_init": 123423424324
# },
# "body": {
# }
# }