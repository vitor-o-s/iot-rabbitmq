# Actions 

import logging
import numpy as np
from ..core.stdMessage import IoTMsg
from ..ticket.busTicket import TicketValue

logger = logging.getLogger(__name__)


class Actions(IoTMsg):
    """
    Parameters
    ----------
    Returns
    -------
    Raises
    ------
    """

    def __init__(self, header, body):
        super().__init__()
        self.header = header
        self.body = body


    def select_actions(self):
        try:
            action = self.header['action'].split('.')
            _action = action[2]
            assert action[0] == "iotrabbitmq" and action[1] == "api" \
                                and _action in self.actions_list, "not a valid action"

            if _action == "ticket_value":
                values = TicketValue()
                values.get_ticket_value()
                self.body.update(values.fare)
                # everything ok
                self.header.update({"code": "000"})
            elif _action == "get_pos":
                print("wip")
            else:
                # not a valid action
                self.header.update({"code": "003"})
        except:
            # ticket value error
            self.header.update({"code":"002"})
        msg = {"header": self.header, "body": self.body}
        return msg