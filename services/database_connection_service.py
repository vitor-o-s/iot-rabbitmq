import psycopg2
# from psycopg2._psycopg import connection


class Connection(object):

    def __init__(self, host, port, usr, pwd, db):

        try:
            self._db = psycopg2.connect(user=usr, password=pwd, host=host, port=port, database=db)

            cursor = self._db.cursor(self)
            # PostgreSQL Connection properties
            print(self._db.get_dsn_parameters(self), '\n')
            # Print PostgreSQL version
            cursor.execute('SELECT version();')
            record = cursor.fetchone()
            print('You are connected to - ', record, '\n')
        except (Exception, psycopg2.Error) as error:
            print('Error ', error, ' while connecting to PostgreSQL')
        finally:
            # closing database connection
            if cursor:
                cursor.close()
                Connection.close(self)
                print('PostgreSQL connection is closed')

    def manipulate(self, sql):

        try:
            cur = self._db.cursor()
            cur.execute(sql)
            cur.close()
            self._db.commmit()
        except(Exception, psycopg2.Error) as error:
            print('Error ', error, ' while connecting to PostgreSQL')
            return False
        return True

    def consult(self, sql):

        # response = None
        try:
            cur = self._db.cursor()
            cur.execute(sql)
            response = cur.fetchall()
        except(Exception, psycopg2.Error) as error:
            print('Error ', error, ' while connecting to PostgreSQL')
            return None
        return response

    def next_primary_key(self, key, table):
        sql = 'SELECT max(' + key + ')from ' + table
        rs = self.consult(sql)
        pk = rs[0][0]
        return pk + 1

    def close(self):
        self._db.close()
