# Get Bus Fare Service

from nameko.rpc import rpc, RpcProxy

class bus_fare:
    name = "ticket_value"

    @rpc
    def get_value(self, ):
        return {'Student':2.2,
                'Normal':1.2,
                'Elderly':0}